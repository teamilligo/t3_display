angular.module('starter.services', [])

.factory('Timeslot', function($http, $ionicLoading) {
    var Timeslot = {
        timeslotlist:[]
    }
  
    Timeslot.refreshTime = function(successcb, failurecb){
         // $ionicLoading.show();
          // var today_date = moment().format('YYYY-MM-DD');
        var today_date = moment('2017-08-15', 'YYYY-MM-DD').format('YYYY-MM-DD');
        // console.log(today_date);
         $http({
                method: 'POST',
                url: apiurl + 'get_pre_reg_timeslots',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    // console.log(str.join("&"));
                    return str.join("&");
                },
                data: {date: today_date}
            }).
            success(function($data, $status, $headers, $config) {
                // console.log($data);
                 $ionicLoading.hide();
                if ($data) {
                    if ($data.code == 200) {
                        $ionicLoading.hide();
                        
                        Timeslot.timeslotlist = $data.data;

                        if (successcb) {
                            successcb($data, $status, $headers, $config);
                        }

                    } else {
                        
                        if (failurecb) {
                            failurecb($data, $status, $headers, $config, $data.description);
                        }
                    }
                }
                // this callback will be called asynchronously
                // when the response is available
            }).error(function($data, $status, $headers, $config) {
                  $ionicLoading.hide();
                //alert('error');
                if (failurecb) {
                    failurecb($data, $status, $headers, $config, "Error During Submission.");
                }
            });
      }

      Timeslot.getTimeslot = function(time_now){
        // var time_now = moment().format('YYYY-MM-DD hh:mm A');
        var time_now = moment().format('2017-08-15 hh:mm A');

        // var date_today = moment().format('YYYY-MM-DD');
        var date_today = moment().format("2017-08-15");


        for(var i = 0 ; i < Timeslot.timeslotlist.length; i++){
          var str = Timeslot.timeslotlist[i].time;
          
          var res = str.substring(0,2);
          // console.log(res <= 12);
          var sign;
          if(res == 12 || (res >= 1 && res <= 8)){
            sign = "PM";
          } else {
            sign = "AM";
          }
         
          Timeslot.timeslotlist[i].group_id = res + ":00 " + sign;
          // console.log(Timeslot.timeslotlist[i].time);
          Timeslot.timeslotlist[i].date_time = date_today + " " + Timeslot.timeslotlist[i].time;
          Timeslot.timeslotlist[i].diff_from_now =   moment(Timeslot.timeslotlist[i].date_time, "YYYY-MM-DD hh:mm A").diff(moment(time_now, "YYYY-MM-DD hh:mm A"), 'minutes');
          Timeslot.timeslotlist[i].time = moment(Timeslot.timeslotlist[i].time, "hh:mm A").format("HH:mm")
            // console.log(Timeslot.timeslotlist[i].date_time);
            // console.log(moment(Timeslot.timeslotlist[i].date_time, "YYYY-MM-DD hh:mm A").diff(moment(time_now, "YYYY-MM-DD hh:mm A"), 'hours'));
        }
        
        // console.log(time_now);
        // console.log(Timeslot.timeslotlist);
        return Timeslot.timeslotlist;
      }

      return Timeslot;
})