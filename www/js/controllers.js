angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('TimeslotCtrl', function($scope, Timeslot, $interval) {
  // var timenow = moment().format('hh:mm A');
  $scope.group_id = [{id:'09:00 AM'}, {id:'10:00 AM'}, {id:'11:00 AM'}, {id:'12:00 PM'}, {id:'01:00 PM'}, {id:'02:00 PM'}, {id:'03:00 PM'}, {id:'04:00 PM'}, {id:'05:00 PM'}]


  $scope.refreshTimeNow = function(){
    $scope.current_time = moment().format("HH:mm");
    $scope.today_date = moment().format("DD MMM YYYY");
  }
  $scope.refreshTimeNow();
  $interval($scope.refreshTimeNow,1000);

  $scope.refreshTime = function(){
    
    var timenow = moment().format('hh:mm A');
    for(var i = 0; i < $scope.group_id.length; i++){
      $scope.group_id[i].hours_diff = moment($scope.group_id[i].id, "hh:mm A").diff(moment(timenow, "hh:00 A"), 'hours');
    }
    // console.log($scope.group_id);

    $scope.timeslots_tmp = Timeslot.getTimeslot(timenow);
    $scope.timeslots = [];

    for (var i = 0; i < $scope.timeslots_tmp.length; i ++) {
      // console.log($scope.timeslots_tmp[i].diff_from_now);
      if ($scope.timeslots_tmp[i].diff_from_now > 0) {
        $scope.timeslots.push($scope.timeslots_tmp[i]);
      }
    }

   


    $scope.timeslots_a = [];
    for (var i = 0; i < $scope.timeslots.length && i < 10; i ++) {
      $scope.timeslots_a.push($scope.timeslots[i]);
    }

    $scope.timeslots_b = [];
    if ($scope.timeslots.length > 10) {
      for (var i = 10; i < $scope.timeslots.length && i < 20; i ++) {
        $scope.timeslots_b.push($scope.timeslots[i]);
      }
    }

    Timeslot.refreshTime(function($data, $status, $headers, $config){
        $scope.timeslots_tmp = Timeslot.getTimeslot(timenow);
        $scope.timeslots = [];

        for (var i = 0; i < $scope.timeslots_tmp.length; i ++) {
          if ($scope.timeslots_tmp[i].diff_from_now > 0) {
            $scope.timeslots.push($scope.timeslots_tmp[i]);
          }
        }

        $scope.timeslots_a = [];
        for (var i = 0; i < $scope.timeslots.length && i < 10; i ++) {
          $scope.timeslots_a.push($scope.timeslots[i]);
        }

        $scope.timeslots_b = [];
        if ($scope.timeslots.length > 10) {
          for (var i = 10; i < $scope.timeslots.length && i < 20; i ++) {
            $scope.timeslots_b.push($scope.timeslots[i]);
          }
        }
    })
  }

  $scope.refreshTime();
  $interval($scope.refreshTime,3000);

})


